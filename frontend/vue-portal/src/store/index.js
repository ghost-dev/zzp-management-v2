import { createStore } from 'vuex';
import AuthService from '@/services/authService';

export default createStore({
  state: {
    auth: {
      isUserLoggedIn: false,
      userToken: null,
      userId: null,
    },
    user: {
      id: null,
    },
  },
  mutations: {
    setUserId(state, userId) {
      state.auth.userId = userId;
    },
    setLoggedIn(state, token) {
      state.auth.userToken = token;
      state.auth.isUserLoggedIn = true;
    },
    setIsUserLoggedIn(state) {
      state.auth.isUserLoggedIn = state;
    },
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      // state.status.loggedIn = false;
      state.user = null;
      state.auth.isUserLoggedIn = false;
      state.auth.userToken = null;
      localStorage.removeItem('token');
    },
    registerSuccess(state) {
      state.status.loggedIn = false;
    },
    registerFailure(state) {
      state.status.loggedIn = false;
    },
  },
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        (user) => {
          commit('loginSuccess', user);

          return Promise.resolve(user);
        },
        (error) => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      // AuthService.logout();
      commit('logout');
    },
    register({ commit }, user) {
      return AuthService.register(user).then(
        (response) => {
          commit('registerSuccess');
          return Promise.resolve(response.data);
        },
        (error) => {
          commit('registerFailure');
          return Promise.reject(error);
        }
      );
    },
  },
  getters: {
    get_token(state) {
      return state.userToken;
    },
    get_userId(state) {
      return state.userId;
    },
    getIsLoggedIn(state) {
      return state.auth.isUserLoggedIn;
    },
    getUserId(state) {
      return state.auth.userId;
    },
  },
  modules: {},
});
