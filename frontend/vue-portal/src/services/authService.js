import axios from 'axios';

const API_URL = process.env.VUE_APP_BASE_URL_LOGINSERVICE;

class AuthService {
  login(user) {
    return axios
      .post(API_URL + '/login', {
        username: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem('token', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('token');
  }

  register(user) {
    return axios.post(API_URL + '/users', {
      username: user.password,
      firstname: user.password,
      lastname: user.lastname,
      password: user.password,
      repeatedPassword: user.repeatedPassword,
      organisationName: user.organisationName,
      address: user.address,
      zipcode: user.zipcode,
      city: user.city,
      agreesPrivacyPolicy: user.agreesPrivacyPolicy,
    });
  }
}

export default new AuthService();
