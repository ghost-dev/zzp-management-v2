export default class User{
    id = 0;
    username = '';
    firstname = '';
    lastname = '';
    organisationId = '';

    constructor(id, username, firstname, lastname,organisationId) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.organisationId = organisationId;
    }

    constructor(username, firstname, lastname,organisationId) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.organisationId = organisationId;
    }

    constructor(username, firstname, lastname) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
    }

}
