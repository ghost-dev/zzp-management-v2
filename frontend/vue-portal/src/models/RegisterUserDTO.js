export default class RegisterUserDTO{
    username = '';
    firstname = '';
    lastname = '';
    password = '';
    repeatedPassword = '';
    organisationName = '';
    address = '';
    zipcode = '';
    city = '';
    agreesPrivacyPolicy = false;

    constructor(username, firstname, lastname, password,repeatedPassword,organisationName,address,zipcode,city, agreesPrivacyPolicy) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.repeatedPassword = repeatedPassword;
        this.organisationName = organisationName;
        this.address = address;
        this.zipcode = zipcode;
        this.city = city;
        this.agreesPrivacyPolicy = agreesPrivacyPolicy;
    }

}
