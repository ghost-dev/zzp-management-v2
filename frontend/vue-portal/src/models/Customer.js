export default class Customer{
    name = '';
    address = '';
    city = '';
    zipcode = '';

    constructor(name, address, city, zipcode) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.zipcode = zipcode;
    }

}
