import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '@/views/Login';
import RelationManagement from '@/views/RelationManagement';
import TimeRegistration from '@/views/TimeRegistration';
import Dashboard from '@/views/Dashboard';
import Register from '@/views/Register';
import Privacy from '@/views/PrivacyPolicy';

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      auth: false,
      title: 'Home',
      fullpage: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      auth: false,
      title: 'Register',
      fullpage: true,
    },
  },
  {
    path: '/timeregistration',
    name: 'Urenregistratie',
    component: TimeRegistration,
    meta: {
      auth: false,
      title: 'Urenregistratie',
      fullpage: false,
    },
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      auth: false,
      title: 'Dashboard',
    },
  },
  {
    path: '/relationmanagement',
    name: 'Relatiebeheer',
    component: RelationManagement,
    meta: {
      auth: false,
      title: 'Relatiebeheer',
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      auth: false,
      title: 'Login',
      fullpage: true,
    },
  },
  {
    path: '/privacy-policy',
    name: 'Privacy Policy',
    component: Privacy,
    meta: {
      auth: false,
      title: 'Privacy Policy',
      fullpage: true,
    },
  },
  {
    path: '/about',
    name: 'About',
    meta: {
      auth: false,
      title: 'About',
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    name: 'Home',
    component: Home,
    meta: {
      auth: false,
      title: 'Home',
      fullpage: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
