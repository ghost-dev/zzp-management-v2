package nl.zzpmanagement.customerservice.service;

import nl.zzpmanagement.customerservice.model.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DataJpaTest
public class CustomerServiceTests {

    @Test
    public void testCreatingCustomer(){

        //Arrange
        List<Customer> customers = new ArrayList<>();
        final String customerName = "Paul";
        final String customerId = UUID.randomUUID().toString();

        //Act
        Customer newCustomer = new Customer();
        newCustomer.setName(customerName);
        newCustomer.setUserId(UUID.fromString(customerId));

        customers.add(newCustomer);


        //Assert

        Assert.isTrue( customers.contains(newCustomer));
        Assert.isTrue(customers.size() > 0,"is true");
        Assert.isTrue(customers.contains(newCustomer));
    }
}
