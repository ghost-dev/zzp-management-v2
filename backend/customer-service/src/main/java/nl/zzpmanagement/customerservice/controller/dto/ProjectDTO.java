package nl.zzpmanagement.customerservice.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO {

    @ApiModelProperty(position = 0, value = "The name of the project", example = "Onderhoud")
    @NotBlank(message = "Name must be filled in")
    private String name;

    @ApiModelProperty(position = 0, value = "The description of the project", example = "Het onderhouden van een systeem")
    @NotBlank(message = "Description must be filled in")
    private String description;

    @ApiModelProperty(position = 0, value = "The customerId of the customer who to the project belongs to", example = "5435435")
    @NotBlank(message = "CustomerId must be filled in")
    private UUID customerId;
}
