package nl.zzpmanagement.customerservice.config.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public interface SecurityContextHolderAccessor {

    /**
     * Sets an {@link Authentication} inside the {@link SecurityContextHolder}
     *
     * @param authentication The {@link Authentication} to be set
     */
    void setSecurityAuthentication(Authentication authentication);
}
