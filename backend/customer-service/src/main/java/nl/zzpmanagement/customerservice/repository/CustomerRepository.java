package nl.zzpmanagement.customerservice.repository;

import nl.zzpmanagement.customerservice.model.Customer;
import nl.zzpmanagement.customerservice.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID> {

    Optional<Customer> findById(UUID id);
    Optional<Customer> findCustomerByNameIgnoreCaseContainingAndUserId(String name, UUID userId);

    Set<Customer> findAllByUserId(UUID userId);


}
