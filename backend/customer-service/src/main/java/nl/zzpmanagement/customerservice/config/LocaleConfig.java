package nl.zzpmanagement.customerservice.config;

import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

@Configuration
public class LocaleConfig {

    @PostConstruct
    public void init() {

        TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of("Europe/Amsterdam")));

        System.out.println("Date in currentTimeZone Europe/Amsterdam: " + new Date().toString());
    }
}
