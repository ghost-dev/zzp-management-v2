package nl.zzpmanagement.customerservice.service;

import nl.zzpmanagement.customerservice.model.Customer;
import nl.zzpmanagement.customerservice.model.Project;
import nl.zzpmanagement.customerservice.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

@Service
public class ProjectService {


    private final ProjectRepository projectRepository;


    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Set<Project> getProjectsByNameAndCustomerId(String projectName, UUID customerId) {
        return projectRepository.findProjectByNameIgnoreCaseContainingAndCustomerId(projectName,customerId);
    }

    public Set<Project> getProjectsByCustomerId(UUID customerId) {
        return projectRepository.findAllByCustomerId(customerId);
    }

    public Project getProjectById(UUID projectId) {
        return projectRepository.findById(projectId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Project with id " + projectId + " not found"));
    }

}
