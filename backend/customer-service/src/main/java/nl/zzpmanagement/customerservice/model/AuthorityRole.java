package nl.zzpmanagement.customerservice.model;

public enum AuthorityRole {
    ROLE_USER,
    ROLE_ADMIN,
}
