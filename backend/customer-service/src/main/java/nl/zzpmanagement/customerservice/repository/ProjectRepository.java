package nl.zzpmanagement.customerservice.repository;

import nl.zzpmanagement.customerservice.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {

    Optional<Project> findById(UUID id);
    Set<Project> findProjectByNameIgnoreCaseContainingAndCustomerId(String name, UUID customerId);

    Set<Project> findAllByCustomerId(UUID customerId);


}
