package nl.zzpmanagement.customerservice.config.security;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import nl.zzpmanagement.customerservice.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class TokenProviderImpl implements TokenProvider {

    private final Logger log = LoggerFactory.getLogger(TokenProviderImpl.class);

    private final static String ROLE_PREFIX = "ROLE";
    private final static String JWT_SECRET_LOCATION = "${jwt.secret}";
    private final static String JWT_DURATION_LOCATION = "${jwt.duration}";

    @Value(JWT_SECRET_LOCATION)
    private String jwtSecret;

    @Value(JWT_DURATION_LOCATION)
    private long jwtDuration;


    @Override
    public String createToken(User user){
        Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put(ROLE_PREFIX, user.getAuthorities().iterator().next().getRole());
        claims.put("userId",user.getId());

        Date issuedAt = new Date();
        Date expireAt = new Date(issuedAt.getTime() + jwtDuration);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(issuedAt)
                .setExpiration(expireAt)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    @Override
    public boolean validateToken(String token){
        try {
            Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token);

            return true;
        } catch (JwtException | IllegalArgumentException e){
            log.info("Invalid JWT provided " +  TokenFilter.getRequestRemoteAddr());
            log.trace("Invalid JWT trace", e);
        }
        return false;
    }

    public Authentication getAuthentication(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        Collection<? extends GrantedAuthority> authorities = Arrays.stream(claims.get(ROLE_PREFIX).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        Object principal =  claims.get("userId");

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    @Override
    public String getUsernameFromToken(String token){
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

}
