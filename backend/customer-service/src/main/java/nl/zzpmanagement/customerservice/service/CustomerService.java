package nl.zzpmanagement.customerservice.service;

import nl.zzpmanagement.customerservice.controller.dto.CustomerDTO;
import nl.zzpmanagement.customerservice.controller.dto.ProjectDTO;
import nl.zzpmanagement.customerservice.model.Customer;
import nl.zzpmanagement.customerservice.model.Project;
import nl.zzpmanagement.customerservice.repository.CustomerRepository;
import nl.zzpmanagement.customerservice.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ProjectRepository projectRepository;


    @Autowired
    public CustomerService(CustomerRepository customerRepository, ProjectRepository projectRepository) {
        this.customerRepository = customerRepository;
        this.projectRepository = projectRepository;
    }

    public Customer getCustomerByNameAndUserId(String name, UUID userId) {
        return customerRepository.findCustomerByNameIgnoreCaseContainingAndUserId(name, userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer with " + name + " and " + " userId " + userId + " not found" ));
    }

    public Customer getCustomerById(UUID id) {
        return customerRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No Customer foud with id: " + id));
    }

    public Customer addCustomer(CustomerDTO customer) {

        if (customerRepository.findCustomerByNameIgnoreCaseContainingAndUserId(customer.getName(), customer.getUserId()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Customer name already exists: " + customer.getName());
        }

        Customer newCustomer = new Customer();
        newCustomer.setName(customer.getName());
        newCustomer.setUserId(customer.getUserId());
        newCustomer.setAddress(customer.getAddress());
        newCustomer.setZipcode(customer.getZipcode());
        newCustomer.setCity(customer.getCity());


        return customerRepository.save(newCustomer);
}

    public Project addProject(ProjectDTO projectDTO) {

        Customer existingCustomer = customerRepository.findById(projectDTO.getCustomerId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Customer doesnt exist: " + projectDTO.getCustomerId()));

        Project newProject = new Project();
        newProject.setName(projectDTO.getName());
        newProject.setDescription(projectDTO.getDescription());
        newProject.setCustomer(existingCustomer);

        existingCustomer.getProjects().add(newProject);

        return projectRepository.save(newProject);
    }

    public Set<Customer> getAllCustomersByUserId(UUID userId) {
        return customerRepository.findAllByUserId(userId);

    }
}
