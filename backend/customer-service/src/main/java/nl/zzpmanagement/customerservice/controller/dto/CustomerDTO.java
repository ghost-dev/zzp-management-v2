package nl.zzpmanagement.customerservice.controller.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class CustomerDTO {

    @ApiModelProperty(position = 0, value = "The name of the customer", example = "Pietje van Overveld")
    @NotBlank(message = "Name must be filled in")
    public String name;

    @ApiModelProperty(position = 0, value = "The address of the customer", example = "Bloemenmarkt 12")
    @NotBlank(message = "Address must be filled in")
    public String address;

    @ApiModelProperty(position = 0, value = "The zipcode of the customer", example = "5031YY")
    @NotBlank(message = "Zipcode must be filled in")
    public String zipcode;

    @ApiModelProperty(position = 0, value = "The city of the customer", example = "Amsterdam")
    @NotBlank(message = "City must be filled in")
    public String city;

    @ApiModelProperty(position = 0, value = "The userId of the user who to the customer belongs to", example = "5435435-fdsfdsfds-ffdsfdsfds-fafsdsfdsf")
    public UUID userId;


}
