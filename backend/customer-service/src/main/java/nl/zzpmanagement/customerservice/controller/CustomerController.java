package nl.zzpmanagement.customerservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.zzpmanagement.customerservice.controller.dto.CustomerDTO;
import nl.zzpmanagement.customerservice.controller.dto.ProjectDTO;
import nl.zzpmanagement.customerservice.service.CustomerService;
import nl.zzpmanagement.customerservice.model.Customer;
import nl.zzpmanagement.customerservice.model.Project;
import nl.zzpmanagement.customerservice.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/customers")
@Api(tags = "Customers")
public class CustomerController {

    private final Logger log = LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService customerService;
    private final ProjectService projectService;



    @Autowired
    public CustomerController(CustomerService customerService, ProjectService projectService) {
        this.customerService = customerService;
        this.projectService = projectService;
    }

    /**
     * Gets an Customer by id
     *
     * @param id The ID of the Customer to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link Customer}
     * @throws NotFoundException {@code 404 (Not found)} if the ID couldn't be linked to a Project
     */
    @ApiOperation(
            value = "Get Customer by id",
            response = Customer.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The Customer matching the ID is returned"),
            @ApiResponse(code = 404, message = "Not found - The Customer matching the id is not found")
    })
    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable UUID id) {
        log.info("[REST]: GET request made to /api/v1/customer/" + id);

        try {
            final Customer returnedCustomer = customerService.getCustomerById(id);

            return ResponseEntity.ok(returnedCustomer);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The Customer matching the id is not found");
        }
    }

    /**
     * Gets an project by id
     *
     * @param id The ID of the project to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link Project}
     * @throws NotFoundException {@code 404 (Not found)} if the ID couldn't be linked to a Project
     */
    @ApiOperation(
            value = "Get project by id",
            response = Project.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The project matching the ID is returned"),
            @ApiResponse(code = 404, message = "Not found - The project matching the id is not found")
    })
    @GetMapping("/projects/{id}")
    public ResponseEntity<?> getProjectById(@PathVariable UUID id) {
        log.info("[REST]: GET request made to /api/v1/customer/" + id);

        try {
            final Project returnedProject = projectService.getProjectById(id);

            return ResponseEntity.ok(returnedProject);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The Project matching the id is not found");
        }
    }

    /**
     * Endpoints that creates a new {@link Customer}
     *
     * @param customer The {@link CustomerDTO} which contains the information about the Customer to create
     * @return The {@link ResponseEntity} with status {@code 201 (Created)} and with body the {@link Customer}
     * @throws BadRequestException {@code 400 (Bad Request)} if the data in the request body couldn't be validated
     * @throws ConflictException   {@code 409 (Conflict)} if the userdata that should be unique, is not unique
     */
    @ApiOperation(
            value = "Create a new Customer"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created - The created Customer is returned"),
            @ApiResponse(code = 400, message = "Bad Request - The data in the request body was not valid"),
            @ApiResponse(code = 409, message = "Conflict - Creation failed due to conflict in Customer's information"),
    })
    @PostMapping("/customers")
    public ResponseEntity<?> addCustomer(@RequestBody CustomerDTO customer) {
        log.info("[REST]: GET request made to /api/v1/customers" );

        try {

            customer.setUserId(UUID.fromString(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString()));

            final Customer returnedCustomer = customerService.addCustomer(customer);

            return ResponseEntity.ok(returnedCustomer);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The data in the request body was not valid");
        }
    }

    /**
     * Endpoints that creates a new {@link Project}
     *
     * @param project The {@link ProjectDTO} which contains the information about the Project to create
     * @return The {@link ResponseEntity} with status {@code 201 (Created)} and with body the {@link Project}
     * @throws BadRequestException {@code 400 (Bad Request)} if the data in the request body couldn't be validated
     * @throws ConflictException   {@code 409 (Conflict)} if the userdata that should be unique, is not unique
     */
    @ApiOperation(
            value = "Create a new Project"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created - The created Project is returned"),
            @ApiResponse(code = 400, message = "Bad Request - The data in the request body was not valid"),
            @ApiResponse(code = 409, message = "Conflict - Creation failed due to conflict in Project's information"),
    })
    @PostMapping("/customers/{customerId}/projects")
    public ResponseEntity<?> addProject(@RequestBody ProjectDTO project) {
        log.info("[REST]: GET request made to /api/v1/customers" );

        try {
            final Project returnedProject = customerService.addProject(project);

            return ResponseEntity.ok(returnedProject);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The data in the request body was not valid");
        }
    }

    /**
     * Gets all Projects by customerId
     *
     * @param id The id of the customer that belong to the projects to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link Project}
     * @throws NotFoundException {@code 404 (Not found)} if the customer Id couldn't be linked to a Project
     */
    @ApiOperation(
            value = "Get all Projects by customerId",
            response = Project.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - All Projects matching the customer id is returned"),
            @ApiResponse(code = 404, message = "Not found - The Projects matching the customer id is not found")
    })
    @GetMapping("/customers/{id}/projects")
    public ResponseEntity<?> getAllProjectsByCustomerId(@PathVariable UUID id) {
        log.info("[REST]: GET request made to /api/v1/customers/" + id +"/projects");

        try {
            final Set<Project> returnedProjects = projectService.getProjectsByCustomerId(id);
            return ResponseEntity.ok(returnedProjects);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The Projects matching the customer id is not found");
        }

    }

    /**
     * Gets all Customers by userId
     *
     * @param userId The userId of the user that belong to the customers to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link Customer}
     * @throws NotFoundException {@code 404 (Not found)} if the userId couldn't be linked to a Customer
     */
    @ApiOperation(
            value = "Get all Customers by userId",
            response = Customer.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - All Customers matching the userId is returned"),
            @ApiResponse(code = 404, message = "Not found - The Customers matching the userId is not found")
    })
    @GetMapping("/users/{userId}/customers")
    public ResponseEntity<?> getCustomerstByUserId(@PathVariable UUID userId) {
        log.info("[REST]: GET request made to /api/v1/users/" + userId +"/customers");

        try {
            final Set<Customer> returnedCustomers = customerService.getAllCustomersByUserId(userId);
            return ResponseEntity.ok(returnedCustomers);

        }catch (HttpServerErrorException exception){
            return ResponseEntity.status(exception.getStatusCode()).body("The Customers matching the userId is not found");
        }

    }
}
