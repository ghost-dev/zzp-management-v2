package nl.zzpmanagement.invoiceservice.service;

import nl.zzpmanagement.invoiceservice.model.Invoice;
import nl.zzpmanagement.invoiceservice.model.InvoiceLine;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.*;

@DataJpaTest
public class InvoiceServiceTest {

    @Test
    public void addInvoice(){
        // Arrange
        List<Invoice> invoiceList = new ArrayList<>();



        // Act
        Invoice invoice = new Invoice();
        String invoiceId = UUID.randomUUID().toString();
        String invoiceNumber  = invoiceId.substring(invoiceId.length() - 12);
        invoice.setInvoiceNumber(invoiceNumber);
        invoice.setCustomerId(UUID.randomUUID());
        invoice.setLocalDateTime(LocalDateTime.now());
        invoice.setProjectId(UUID.randomUUID());


        Set<InvoiceLine> invoiceLines = new HashSet<>(Arrays.asList(
                new InvoiceLine("onderhoud", 20, 15.50, UUID.fromString(invoiceId)),
                new InvoiceLine("werk", 5, 1.50, UUID.fromString(invoiceId)),
                new InvoiceLine("verf", 1, 35.0, UUID.fromString(invoiceId)),
                new InvoiceLine("timmer", 2, 3.50, UUID.fromString(invoiceId)))
        );
        invoice.setInvoiceLineSet(invoiceLines);
        invoiceList.add(invoice);



        // Assert

        Assert.isTrue(invoice.invoiceLineSet.size() == 4);
    }
}
