package nl.zzpmanagement.invoiceservice.controller;

import nl.zzpmanagement.invoiceservice.config.security.IAuthenticationFacade;
import nl.zzpmanagement.invoiceservice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.UUID;

@RestController
public class PdfController {

    private final InvoiceService invoiceService;
    private IAuthenticationFacade authenticationFacade;


    @Autowired
    public PdfController(InvoiceService invoiceService, IAuthenticationFacade authenticationFacade) {
        this.invoiceService = invoiceService;
        this.authenticationFacade = authenticationFacade;
    }


//    @GetMapping(path = "/generatePDF/{userId}")
//    public ResponseEntity<InputStreamResource> generatePDF(@PathVariable UUID userId) throws FileNotFoundException {
//        Authentication authentication = authenticationFacade.getAuthentication();
//        authentication.getName();
//        File generatedPDF = invoiceService.generatePDF(userId);
//        InputStreamResource resource = new InputStreamResource(new FileInputStream(generatedPDF));
//        return ResponseEntity.ok()
//                .contentLength(generatedPDF.length())
//                .contentType(MediaType.APPLICATION_PDF)
//                .body(resource);
//    }
}
