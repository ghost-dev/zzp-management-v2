package nl.zzpmanagement.invoiceservice.model;

import java.util.UUID;


public interface ISalable {

     Boolean isBilled ();

     UUID getInvoiceId();

     Integer getAmount();

     String getDescription();

     SalableType getSalableType();



}
