package nl.zzpmanagement.invoiceservice.model;

public enum AuthorityRole {
    ROLE_USER,
    ROLE_ADMIN,
}
