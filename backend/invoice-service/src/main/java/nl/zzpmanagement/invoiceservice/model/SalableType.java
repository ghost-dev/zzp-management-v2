package nl.zzpmanagement.invoiceservice.model;

public enum SalableType {
    PRODUCT,
    LABOR
}
