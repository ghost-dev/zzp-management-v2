package nl.zzpmanagement.invoiceservice.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class InvoiceLineDTO {
 
  private UUID id;

  public String description;

  public Integer amount;

  public Double sellingPrice;

  public UUID invoiceId;
}
