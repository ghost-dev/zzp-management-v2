package nl.zzpmanagement.invoiceservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
public class InvoiceLine {

    @Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    public String description;

    public Integer amount;

    public Double sellingPrice;

    public Double purchasingPrice;

//    @ManyToOne
//    public ISalable salable;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    private Invoice invoice;

    @Column(name = "invoice_id", insertable = false, updatable = false)
    private UUID invoiceId;


    public InvoiceLine(String description, Integer amount, Double sellingPrice, UUID invoiceId) {
        this.description = description;
        this.amount = amount;
        this.sellingPrice = sellingPrice;
        this.invoiceId = invoiceId;
    }
}
