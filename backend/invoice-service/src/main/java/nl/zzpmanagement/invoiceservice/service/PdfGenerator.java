package nl.zzpmanagement.invoiceservice.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import nl.zzpmanagement.invoiceservice.controller.dto.InvoiceLineDTO;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

@Service
public class PdfGenerator {

    private final String FILENAME = "factuur.pdf";
    public File generatePDF(java.util.List<InvoiceLineDTO> invoiceLineDTOList) throws IOException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FILENAME));

        document.open();


        //add logo
        String imageFile = "logo_be.png";
        Image img = Image.getInstance(imageFile);

        img.scaleAbsolute(150f, 50f);
        img.setAlignment(Image.ALIGN_RIGHT);
        document.add(img);

        Paragraph paragraph = new Paragraph("Naam: Jan Jaap");
        document.add(paragraph);
        paragraph = new Paragraph("Adres: Beneluxstraat 123");
        document.add(paragraph);
        paragraph = new Paragraph("Woonplaats: 98484 Antwerpen");
        document.add(paragraph);

        //add title
        Font font = FontFactory.getFont(FontFactory.COURIER_BOLD, 23, BaseColor.BLACK);
        paragraph = new Paragraph("Factuurnummer: 234324", font);
        document.add(paragraph);paragraph = new Paragraph("Beschrijving", font);
        document.add(paragraph);

        //add list of bills to be paid
        com.itextpdf.text.List list = new com.itextpdf.text.List(false);
        for (InvoiceLineDTO invoiceLineDTO: invoiceLineDTOList) {

            list.add("Totaal aantal kilometers van rit was : " + invoiceLineDTO.getAmount() + "      €   " + (invoiceLineDTO.getSellingPrice()));
        }
        document.add(list);
        document.close();

        return new File(FILENAME);

    }
}
