package nl.zzpmanagement.invoiceservice.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InvoiceDTO {
  
  private UUID id;

  public String description;

  public String status;

  public String invoiceNumber;

  public LocalDateTime localDateTime;

  public Boolean isPaid = false;

  public Double price = 0.0;

  public List<InvoiceLineDTO> invoiceLineSet = new ArrayList();
}
