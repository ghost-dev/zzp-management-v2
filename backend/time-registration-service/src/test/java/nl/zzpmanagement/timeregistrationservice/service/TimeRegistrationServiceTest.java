package nl.zzpmanagement.timeregistrationservice.service;

import nl.zzpmanagement.timeregistrationservice.model.WorkedHours;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DataJpaTest
public class TimeRegistrationServiceTest {

    @Test
    public void testCreatingWorkedHours(){

        //Arrange
        List<WorkedHours> workedHours = new ArrayList<>();
        final String workedHoursDescription = "Worked on dashboard";
        final UUID projectId = UUID.randomUUID();
        final UUID userId = UUID.randomUUID();

        //Act
        WorkedHours workedHour = new WorkedHours();
        workedHour.setDescription(workedHoursDescription);
        workedHour.setProjectId(projectId);
        workedHour.setUserId(userId);


        workedHours.add(workedHour);


        //Assert

        Assert.isTrue( workedHours.contains(workedHour));
        Assert.isTrue(workedHours.size() > 0,"is true");
        Assert.isTrue(workedHours.contains(workedHour));
    }
}
