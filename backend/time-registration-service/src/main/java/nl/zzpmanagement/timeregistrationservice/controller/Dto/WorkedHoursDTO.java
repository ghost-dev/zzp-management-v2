package nl.zzpmanagement.timeregistrationservice.controller.Dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
public class WorkedHoursDTO {

    public UUID projectId;

    public String description;

    public String usernameWorker;

    public LocalDateTime startTime;

    public LocalDateTime endTime;
}
