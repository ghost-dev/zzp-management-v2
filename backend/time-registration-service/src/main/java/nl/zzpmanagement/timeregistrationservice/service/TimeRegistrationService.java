package nl.zzpmanagement.timeregistrationservice.service;

import nl.zzpmanagement.timeregistrationservice.controller.Dto.WorkedHoursDTO;
import nl.zzpmanagement.timeregistrationservice.model.WorkedHours;
import nl.zzpmanagement.timeregistrationservice.repository.WorkedHoursRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Set;
import java.util.UUID;

@Service
public class TimeRegistrationService {

    private final WorkedHoursRepository workedHoursRepository;

    @Autowired
    public TimeRegistrationService(WorkedHoursRepository workedHoursRepository) {
        this.workedHoursRepository = workedHoursRepository;
    }


    @Transactional
    public WorkedHours addWorkedHours(WorkedHoursDTO workedHoursDTO, UUID userId){

        WorkedHours newWorkedHours = new WorkedHours();
        newWorkedHours.setDescription(workedHoursDTO.getDescription());
        newWorkedHours.setStartTime(workedHoursDTO.getStartTime());
        newWorkedHours.setEndTime(workedHoursDTO.getEndTime());
        newWorkedHours.setUserId(userId);
        newWorkedHours.setProjectId(workedHoursDTO.getProjectId());

        return workedHoursRepository.save(newWorkedHours);
    }

    public WorkedHours getWorkedHoursById(UUID workedHoursId){
        return workedHoursRepository.findById(workedHoursId).orElseThrow( () ->  new HttpServerErrorException(HttpStatus.CONFLICT,"The passwords did not match"));
    }

    public Set<WorkedHours> getAllWorkedHoursByUserId(UUID userId){
        return workedHoursRepository.findAllByUserId(userId);
    }
}
