package nl.zzpmanagement.timeregistrationservice.config.security;

import nl.zzpmanagement.timeregistrationservice.model.User;
import org.springframework.security.core.Authentication;

public interface TokenProvider {

    /**
     * Creates a new JWT based on a given {@link User}
     *
     * @param user The {@link User} for who the JWT is created
     * @return The created JWT
     */
    String createToken(User user);

    /**
     * Validates a given JWT
     *
     * @param token The JWT to be validated
     * @return true if valid, otherwise false
     */
    boolean validateToken(String token);


    /**
     * Returns the {@link Authentication} based on a given token
     *
     * @param token The JWT
     * @return {@link Authentication} of the user to which to JWT belongs.
     */
    Authentication getAuthentication(String token);

    /**
     * Retrieves the username from the JWT
     *
     * @param token The JWT
     * @return The username of the user to which the JWT belongs
     */
    String getUsernameFromToken(String token);
}
