package nl.zzpmanagement.timeregistrationservice.model;

public enum AuthorityRole {
    ROLE_USER,
    ROLE_ADMIN,
}
