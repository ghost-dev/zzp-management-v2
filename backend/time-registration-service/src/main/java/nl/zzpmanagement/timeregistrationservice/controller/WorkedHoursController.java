package nl.zzpmanagement.timeregistrationservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.zzpmanagement.timeregistrationservice.controller.Dto.WorkedHoursDTO;
import nl.zzpmanagement.timeregistrationservice.model.User;
import nl.zzpmanagement.timeregistrationservice.model.WorkedHours;
import nl.zzpmanagement.timeregistrationservice.service.TimeRegistrationService;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.UUID;

@RestController
@Api(tags = "WorkedHours")
@RequestMapping("/workedhours")
public class WorkedHoursController {

    private static final Logger log = LoggerFactory.getLogger(WorkedHoursController.class);
    private final static String URL_LOGIN_SERVICE_BASE = "${url.login.service.base}";
    private final static String URL_CUSTOMER_SERVICE_BASE = "${url.customer.service.base}";

    @Value(URL_LOGIN_SERVICE_BASE)
    private String urlLoginService;

    @Value(URL_CUSTOMER_SERVICE_BASE)
    private String urlCustomerService;

    private RestTemplate restTemplate;

    private TimeRegistrationService timeRegistrationService;

    @Autowired
    public WorkedHoursController(RestTemplate restTemplate, TimeRegistrationService timeRegistrationService) {
        this.restTemplate = restTemplate;
        this.timeRegistrationService = timeRegistrationService;
    }

    /**
     * {@code POST: /workedhours}: V
     *
     * @param WorkedHoursDTO The {@link WorkedHoursDTO} which contains the information to perform the WorkedHours request
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} and with body the WorkedHours
     * @throws BadRequestException {@code 400 (Bad Request)} The provided WorkedHours information is not correct.
     */
    @ApiOperation(
            value = "Adds new workedhours to time registration"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The workedhours is succesful registrated"),
            @ApiResponse(code = 400, message = "Bad Request - The provided information is not correct")
    })
    @PostMapping("/workedhours")
    public ResponseEntity<?> addWorkedHours(@Valid @RequestBody WorkedHoursDTO workedHoursDTO, @RequestHeader (name="Authorization") String token) {
        log.info("[REST]: POST request made to workedhours route: /workedhours");
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            HttpEntity<String> entity = new HttpEntity<String>(headers);

            log.info("[REST]: GET request made to authservice route: finduserbyusername");

            User user = restTemplate.exchange(urlLoginService + "/users/finduserbyusername/" + workedHoursDTO.getUsernameWorker() , HttpMethod.GET,entity, User.class).getBody();

            //            User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            if(user != null && user.getId() != null) {

                log.info("[REST]: GET request made to customerservice route: /projects");

                Object project = restTemplate.exchange(urlCustomerService + "/customers/projects/" + workedHoursDTO.getProjectId() , HttpMethod.GET,entity, Object.class).getBody();
                UUID projectId = UUID.fromString(BeanUtils.getProperty(project,"id"));
                workedHoursDTO.setProjectId(projectId);

                return ResponseEntity.status(HttpStatus.OK).body(timeRegistrationService.addWorkedHours(workedHoursDTO, user.getId()));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incorrecte information.");

        } catch (HttpServerErrorException | HttpClientErrorException exception) {
            log.info("[REST]: POST request made to workedhours route: /workedhours" );

            return ResponseEntity.status(exception.getStatusCode()).body("Incorrecte information.");
        } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incorrecte information.");
        }
    }




    /**
     * Gets an WorkedHours by id
     *
     * @param id The ID of the WorkedHours to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link WorkedHours}
     * @throws NotFoundException {@code 404 (Not found)} if the ID couldn't be linked to a Donation
     */
    @ApiOperation(
            value = "Get WorkedHours by id",
            response = WorkedHours.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The WorkedHours matching the ID is returned"),
            @ApiResponse(code = 404, message = "Not found - The WorkedHours matching the id is not found")
    })
    @GetMapping("/workedhours/{id}")
    public ResponseEntity<?> getWorkedHoursById(@PathVariable UUID id) {
        log.info("[REST]: GET request made to /workedhours/" + id );

        final WorkedHours returnedWorkedHours = timeRegistrationService.getWorkedHoursById(id);

        return ResponseEntity.ok(returnedWorkedHours);
    }

    /**
     * Gets an WorkedHours by userId
     *
     * @param id The userId of the WorkedHours to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link WorkedHours}
     * @throws NotFoundException {@code 404 (Not found)} if the ID couldn't be linked to a Donation
     */
    @ApiOperation(
            value = "Get WorkedHours by id",
            response = WorkedHours.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The WorkedHours matching the ID is returned"),
            @ApiResponse(code = 404, message = "Not found - The WorkedHours matching the id is not found")
    })
    @GetMapping("/users/{id}/workedhours")
    public ResponseEntity<?> getAllWorkedHoursByUserId(@PathVariable UUID id) {
        log.info("`[REST]: GET request made to /users/"+ id +"/workedhours`" );

        final Set<WorkedHours> returnedWorkedHours = timeRegistrationService.getAllWorkedHoursByUserId(id);

        return ResponseEntity.ok(returnedWorkedHours);
    }
}
