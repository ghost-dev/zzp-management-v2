package nl.zzpmanagement.timeregistrationservice.repository;

import nl.zzpmanagement.timeregistrationservice.model.WorkedHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface WorkedHoursRepository extends JpaRepository<WorkedHours, UUID> {

    Optional<WorkedHours> findById(UUID id);

    Set<WorkedHours> findAllByUserId(UUID userId);

    Set<WorkedHours> findAllByProjectId(UUID projectId);

    Set<WorkedHours> findAllByUserIdAndProjectId(UUID userId, UUID projectId);

    Set<WorkedHours> findAllByInvoiceId(UUID invoiceId);




}
