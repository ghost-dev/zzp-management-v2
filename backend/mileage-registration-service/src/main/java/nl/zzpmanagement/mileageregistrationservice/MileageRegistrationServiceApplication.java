package nl.zzpmanagement.mileageregistrationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MileageRegistrationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MileageRegistrationServiceApplication.class, args);
	}

}
