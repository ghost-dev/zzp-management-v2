package nl.zzpmanagement.loginservice.service;

import nl.zzpmanagement.loginservice.controller.dto.LoginDTO;
import nl.zzpmanagement.loginservice.controller.dto.RegisterUserDTO;
import nl.zzpmanagement.loginservice.controller.dto.UpdatePasswordDTO;
import nl.zzpmanagement.loginservice.model.Authority;
import nl.zzpmanagement.loginservice.model.AuthorityRole;
import nl.zzpmanagement.loginservice.model.User;
import nl.zzpmanagement.loginservice.model.UserDetailsImpl;
import nl.zzpmanagement.loginservice.repository.AuthorityRepository;
import nl.zzpmanagement.loginservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@Service
public class AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;


    @Autowired
    public AuthService(PasswordEncoder passwordEncoder, UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.createDefaultRoles();
    }

    public void createDefaultRoles(){

        List<Authority> authorities = authorityRepository.findAll();

        if(authorities.size() < 1){
            authorities.add(new Authority(AuthorityRole.ROLE_ADMIN));
            authorities.add(new Authority(AuthorityRole.ROLE_USER));
            authorityRepository.saveAll(authorities);
        }
    }

    public UserDetails getUserDetailsByUsername(String username) {
        User user = userRepository
                .findByUsername(username)
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

        return UserDetailsImpl.builder(user);
    }

    public UserDetails getUserDetailsByUserId(UUID uuid) {
        User user = userRepository
                .findById(uuid)
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

        return UserDetailsImpl.builder(user);
    }

    public User getUserByUsername(String username){
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

    }

    public User login(LoginDTO loginDTO) {

        User user = userRepository
                .findByUsername(loginDTO.getUsername())
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

        if (passwordEncoder.matches(loginDTO.getPassword(), user.getPassword())) {
            return user;
        } else {
            throw new HttpServerErrorException(HttpStatus.CONFLICT,"The entered password is not correct");
        }
    }

    public User register(RegisterUserDTO registerUserDTO) {

        if (!registerUserDTO.getPassword().equals(registerUserDTO.getRepeatedPassword())) {
            throw new HttpServerErrorException(HttpStatus.CONFLICT,"The passwords did not match");
        } else if (userRepository.findByUsername(registerUserDTO.getUsername()).isPresent()) {
            throw new HttpServerErrorException(HttpStatus.CONFLICT,"User with this email already exists");
        } else if (registerUserDTO.getPassword().length() < 8) {
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST,"Insecure password, password should be over 4 characters");
        } else {
            User user = new User();
            user.setUsername(registerUserDTO.getUsername());
            user.setPassword(passwordEncoder.encode(registerUserDTO.getPassword()));
            user.setFirstname(registerUserDTO.firstname);
            user.setLastname(registerUserDTO.lastname);
            user.setAddress(registerUserDTO.address);
            user.setZipcode(registerUserDTO.zipcode);
            user.setCity(registerUserDTO.city);
            user.setOrganisationName(registerUserDTO.organisationName);
            // if (registerUserDTO.getRole() == AuthorityRole.ROLE_ADMIN) {
            //     Authority authority = authorityRepository.findAuthorityByRole(AuthorityRole.ROLE_ADMIN)
            //             .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"Role couldn't be found"));
            //     user.setAuthorities(new HashSet<>(Collections.singletonList(authority)));
            // } else {
                Authority authority = authorityRepository.findAuthorityByRole(AuthorityRole.ROLE_USER)
                        .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"Role couldn't be found"));
                user.setAuthorities(new HashSet<>(Collections.singletonList(authority)));

            // }

            user = userRepository.save(user);

            return user;
        }
    }

    public User updatePasswordPortalUser(String username, UpdatePasswordDTO updatePasswordUserDTO) {
        User user = userRepository
                .findByUsername(updatePasswordUserDTO.getUsername())
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

//        if (!user.getUsername().equals(updatePasswordUserDTO.getUsername())) {
//            throw new UnauthorizedException("You cannot update somebody else's account!");
//        }
        if (!updatePasswordUserDTO.getPassword().equals(updatePasswordUserDTO.getRepeatedPassword())) {
            throw new HttpServerErrorException(HttpStatus.CONFLICT,"The passwords did not match");
        }
        if (!passwordEncoder.matches(updatePasswordUserDTO.getCurrentPassword(), user.getPassword())) {
            throw new HttpServerErrorException(HttpStatus.CONFLICT,"The passwords did not match");
        }
        user.setPassword(passwordEncoder.encode(updatePasswordUserDTO.getPassword()));

        return userRepository.save(user);
    }

}
