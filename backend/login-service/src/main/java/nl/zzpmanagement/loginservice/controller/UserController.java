package nl.zzpmanagement.loginservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.zzpmanagement.loginservice.service.AuthService;
import nl.zzpmanagement.loginservice.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

@RestController
@Api(tags = "Users")
@RequestMapping("/users")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);


    private AuthService authService;

    @Autowired
    public UserController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * Gets an User by username
     *
     * @param username The username of the User to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link User}
     * @throws HttpServerErrorException {@code 404 (Not found)} if the ID couldn't be linked to a Donation
     */
    @ApiOperation(
            value = "Get WorkedHours by id",
            response = User.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The WorkedHours matching the ID is returned"),
            @ApiResponse(code = 404, message = "Not found - The WorkedHours matching the id is not found")
    })
    @GetMapping("/finduserbyusername/{username}")
    public ResponseEntity<?> getWorkedHoursById(@PathVariable String username) {
        log.info("[REST]: GET request made to /users/finduserbyusername/" + username );


        try {

            final User returnedUser = authService.getUserByUsername(username);

            return ResponseEntity.status(HttpStatus.OK).body(returnedUser);

        } catch (HttpServerErrorException exception) {
            log.info("[REST]: GET request made to /users/findbyusername/" + username + " returns not found");
            return ResponseEntity.status(exception.getStatusCode()).body("Incorrecte information.");
        }
    }


    /**
     * Gets an User by username
     *
     * @param username The username of the User to find
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} with as body a list of {@link User}
     * @throws HttpServerErrorException {@code 404 (Not found)} if the ID couldn't be linked to a Donation
     */
    @ApiOperation(
            value = "Get userid by username",
            response = User.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The userid matching the username is returned"),
            @ApiResponse(code = 404, message = "Not found - The user matching the username is not found")
    })
    @GetMapping("/finduseridbyusername/{username}")
    public ResponseEntity<?> getUserIdByUsername(@PathVariable String username) {
        log.info("[REST]: GET request made to /users/finduseridbyusername/" + username );


        try {

            final User returnedUser = authService.getUserByUsername(username);

            return ResponseEntity.status(HttpStatus.OK).body(returnedUser.getId());

        } catch (HttpServerErrorException exception) {
            log.info("[REST]: GET request made to /users/finduseridbyusername/" + username + " returns not found");
            return ResponseEntity.status(exception.getStatusCode()).body("Incorrecte information.");
        }
    }
}
