package nl.zzpmanagement.loginservice.Config.security;

import javax.servlet.http.HttpServletRequest;

public interface HeaderAccessor {

    /**
     * Takes a {@link HttpServletRequest} and retrieves a header value from it by name
     *
     * @param req The {@link HttpServletRequest}
     * @param headerName The name of the header
     * @return The value of the header, if the header doesn't exists null
     */
    String getValueFromHeader(HttpServletRequest req, String headerName);

}
