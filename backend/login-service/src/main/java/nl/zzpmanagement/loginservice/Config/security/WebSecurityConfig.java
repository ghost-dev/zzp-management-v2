package nl.zzpmanagement.loginservice.Config.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenFilterConfig tokenFilterConfig;
    private final UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(TokenFilterConfig tokenFilterConfig, UserDetailsServiceImpl userDetailsService){
        this.tokenFilterConfig = tokenFilterConfig;
        this.userDetailsService = userDetailsService;
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {

        return  userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService())
                .passwordEncoder(new BCryptPasswordEncoder(10));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Enable cors configurations and enforcing strict origin control
        // Disable csrf, we are using another token mechanism: JWT
        http.cors().and().csrf().disable();
        http.headers().referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);

        // Make sure spring security doesn't create or use any session, REST is stateless
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Frame options are disabled because the H2 database console can't render if frameoptions are blocked.
        http.headers().frameOptions().disable();

        // Authentication permissions
        http.authorizeRequests()


                .antMatchers("/console/**","/webjars/**").permitAll()
                .antMatchers("/api-doc","/swagger-ui/**",
                        "/v2/api-docs","/v2/api-docs/**","/swagger-ui.html", "/swagger-resources/**").permitAll()
                .antMatchers("/auth/**","/actuator/**").permitAll()
                .antMatchers("/change-pwd").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
                .anyRequest().authenticated().and().apply(tokenFilterConfig);

        // Add jwt filter before request
//        http.apply(tokenFilterConfig);
    }


    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList("" +
                        "http://localhost:4200",
                "http://localhost:4201",
                "http://localhost:80",
                "http://localhost:8080",
                "https://localhost:3000",
                "http://localhost:9090",
                "http://localhost:9091",
                "http://localhost:3000",
                "http://localhost:30000",
                "https://localhost:30000",
                "https://zzp-management.nl",
                "http://zzp-management.nl"

        ));
//        config.set("*");
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

    @Bean
    public CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter
                = new CommonsRequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(false);
        filter.setAfterMessagePrefix("REQUEST DATA : ");
        return filter;
    }
}

