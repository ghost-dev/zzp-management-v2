package nl.zzpmanagement.loginservice.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.zzpmanagement.loginservice.controller.validation.FieldMatch;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@FieldMatch(first = "password", second = "repeatedPassword", message = "The password fields must match")
public class UpdatePasswordDTO {

    @Email
    @ApiModelProperty(position = 1, name = "username")
    @NotBlank(message = "Username must be filled in with an known uuid")
    private String username;

    @ApiModelProperty(name = "the current password of the user", position = 2, example = "oldpassword")
    @NotBlank(message = "Password must be filled in")
    @Size(min = 8, max = 60, message = "Password must be within 8 to 60 characters")
    private String currentPassword;

    @ApiModelProperty(name = "the new password of the user", position = 3, example = "mypassword")
    @NotBlank(message = "Password must be filled in")
    @Size(min = 8, max = 60, message = "Password must be within 8 to 60 characters")
    private String password;

    @ApiModelProperty(name = "the new password of the user", position = 4, example = "mypassword")
    @NotBlank(message = "Password repeat must be filled in")
    private String repeatedPassword;
}
