package nl.zzpmanagement.loginservice.Config.security;


import nl.zzpmanagement.loginservice.model.UserDetailsImpl;
import nl.zzpmanagement.loginservice.model.User;
import nl.zzpmanagement.loginservice.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository
                .findByUsername(s)
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND,"User couldn't be found"));

        return UserDetailsImpl.builder(user);
    }
}
