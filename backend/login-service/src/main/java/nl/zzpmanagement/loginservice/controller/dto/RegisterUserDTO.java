package nl.zzpmanagement.loginservice.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.zzpmanagement.loginservice.controller.validation.FieldMatch;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@FieldMatch(first = "password", second = "repeatedPassword", message = "The password fields must match")
public class RegisterUserDTO {

    @ApiModelProperty(position = 1, name = "username")
    @NotBlank(message = "Username must be filled in with an unknown username")
    public String username;

    @ApiModelProperty(position = 2, name = "firstname")
    @NotBlank(message = "Firstname must be filled in")
    public String firstname;

    @ApiModelProperty(position = 3, name = "lastname")
    @NotBlank(message = "Lastname must be filled in")
    public String lastname;

    @ApiModelProperty(position = 4, name = "gender")
    @NotBlank(message = "Gender must be filled in with an known sex M or V")
    public String gender;

    @ApiModelProperty(name = "the password of the user", position = 5, example = "mypassword")
    @NotBlank(message = "Password must be filled in")
    @Size(min = 8, max = 60, message = "Password must be within 8 to 60 characters")
    public String password;

    @ApiModelProperty(name = "the password of the user", position = 6, example = "mypassword")
    @NotBlank(message = "Password repeat must be filled in")
    public String repeatedPassword;

    @ApiModelProperty(name = "the organisation of the user", position = 7, example = "Van der Linden BV")
    @NotBlank(message = "Organisation must be filled in")
    public String organisationName;

    @ApiModelProperty(name = "the address of the user", position = 8, example = "Bloemenmarkt 12")
    @NotBlank(message = "Address must be filled in")
    public String address;

    @ApiModelProperty(name = "the zipcode of the user", position = 9, example = "5031TT")
    @NotBlank(message = "Zipcode must be filled in")
    public String zipcode;

    @ApiModelProperty(name = "the city of the user", position = 10, example = "Amsterdam")
    @NotBlank(message = "City must be filled in")
    public String city;

    // @ApiModelProperty(name = "the role of the user", position = 7, example = "ROLE_USER")
    // private AuthorityRole role;
}
