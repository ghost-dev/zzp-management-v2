package nl.zzpmanagement.loginservice.model;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@AllArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private String username;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * Builds a UserDetailsImpl object based on a specific user
     * @param user The user model
     * @return An UserDetailsImpl object
     */
    public static UserDetailsImpl builder(User user) {
        String[] roles = new String[] {getUserRole(user)};
        return new UserDetailsImpl(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(roles));
    }

    /**
     * Get the role of the user
     * @param user The user of which the role must be found
     * @return The role of the user in string format
     */
    private static String getUserRole(User user){
        return user.getAuthorities().iterator().next().getRole().name();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
