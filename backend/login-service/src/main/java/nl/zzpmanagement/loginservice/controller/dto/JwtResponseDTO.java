package nl.zzpmanagement.loginservice.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class JwtResponseDTO {
    @ApiModelProperty(position = 0, name = "The JWT")
    private String jwt;
}
