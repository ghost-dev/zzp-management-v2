package nl.zzpmanagement.loginservice.model;

public enum AuthorityRole {
    ROLE_USER,
    ROLE_ADMIN,
}
