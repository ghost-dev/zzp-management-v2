package nl.zzpmanagement.loginservice.repository;

import nl.zzpmanagement.loginservice.model.Authority;
import nl.zzpmanagement.loginservice.model.AuthorityRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findAuthorityByRole(AuthorityRole role);

}
