package nl.zzpmanagement.loginservice.Config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TokenFilter extends OncePerRequestFilter implements HeaderAccessor, SecurityContextHolderAccessor {

    private final static String AUTHORIZATION_HEADER_NAME = "Authorization";
    private final static String BEARER_TOKEN_PREFIX = "Bearer ";

    private TokenProvider tokenProvider;

    @Autowired
    public TokenFilter(TokenProviderImpl jwtProvider){
        this.tokenProvider = jwtProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain filterChain) throws IOException, ServletException
    {
        final String token = getValueFromHeader(req, AUTHORIZATION_HEADER_NAME);

        if(token != null && tokenProvider.validateToken(token)){
            setSecurityAuthentication(tokenProvider.getAuthentication(token));
        }

        filterChain.doFilter(req, res);
    }

    @Override
    public String getValueFromHeader(HttpServletRequest req, String headerName) {
        final String token = req.getHeader(headerName);

//        if (token.isEmpty() || !token.startsWith("Bearer ")) {
//            return null;
//        }
        if(token == null || token.isEmpty()){
            return null;
        }

        return token.replace(BEARER_TOKEN_PREFIX, "");
    }

    @Override
    public void setSecurityAuthentication(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public static String getRequestRemoteAddr(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        return request.getRemoteAddr();
    }
}
