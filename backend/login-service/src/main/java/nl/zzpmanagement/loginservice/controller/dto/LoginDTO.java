package nl.zzpmanagement.loginservice.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class LoginDTO {

    @ApiModelProperty(position = 1, name = "username")
    @NotBlank(message = "Username must be filled in")
    private String username;

    @ApiModelProperty(position = 2, name = "password")
    @NotBlank(message = "Password must be filled in")
    private String password;
}
