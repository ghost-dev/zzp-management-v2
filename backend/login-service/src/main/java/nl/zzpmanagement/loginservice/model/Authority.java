package nl.zzpmanagement.loginservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Authority {

    @JsonIgnore
    @Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    public  AuthorityRole role;

    @ManyToMany(mappedBy = "authorities")
    @JsonIgnore
    private Set<User> users;

    public Authority(AuthorityRole role){
        this.role = role;
    }

    public Authority() {

    }
}
