package nl.zzpmanagement.loginservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.zzpmanagement.loginservice.service.AuthService;
import nl.zzpmanagement.loginservice.Config.security.TokenProviderImpl;
import nl.zzpmanagement.loginservice.controller.dto.JwtResponseDTO;
import nl.zzpmanagement.loginservice.controller.dto.LoginDTO;
import nl.zzpmanagement.loginservice.controller.dto.RegisterUserDTO;
import nl.zzpmanagement.loginservice.controller.dto.UpdatePasswordDTO;
import nl.zzpmanagement.loginservice.model.User;
import nl.zzpmanagement.loginservice.model.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import javax.validation.Valid;

@RestController
@Api(tags = "Authentication")
@RequestMapping("/auth")
public class AuthenticationController {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

    private AuthService authService;
    private TokenProviderImpl tokenProvider;


    @Autowired
    public AuthenticationController(AuthService authService, TokenProviderImpl tokenProvider) {

        this.authService = authService;
        this.tokenProvider = tokenProvider;
    }

    /**
     * {@code POST: /login}: Validates user credentials and returns a JWT if valid.
     *
     * @param loginDTO The {@link LoginDTO} which contains the information to perform the login request
     * @return The {@link ResponseEntity} with status {@code 200 (OK)} and with body the JWT
     * @throws BadRequestException {@code 400 (Bad Request)} The provided login information is not correct.
     */
    @ApiOperation(
            value = "Performs a login action"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The login is succesful, a token is returned"),
            @ApiResponse(code = 400, message = "Bad Request - The provided login information is not correct")
    })
    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginDTO loginDTO) {
        log.info("[REST]: POST request made to login route: /login");
        try {
            User user = authService.login(loginDTO);
            JwtResponseDTO jwtResponse = new JwtResponseDTO(tokenProvider.createToken(user));
            return ResponseEntity.status(HttpStatus.OK).body(jwtResponse);
        } catch (HttpServerErrorException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body("Incorrecte email of wachtwoord.");
        }
    }

    /**
     * {@code POST: /change-pwd}: Validates user credentials for changing password.
     *
     * @param updatePasswordUserDTO The {@link UpdatePasswordUserDTO} which contains the information to perform the change password request
     * @return The {@link ResponseEntity} with status {@code 200 (OK)}
     * @throws BadRequestException {@code 400 (Bad Request)} The provided information is not correct.
     */
    @ApiOperation(
            value = "Performs a change password action"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok - The password is successfully changed"),
            @ApiResponse(code = 400, message = "Bad Request - The provided user information is not correct")
    })
    @PostMapping("/change-pwd")
    public ResponseEntity<?> UpdatePassword(@Valid @RequestBody UpdatePasswordDTO updatePasswordUserDTO) {
        log.info("[REST]: POST request made to login route: /change-pwd");

        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            User user = authService.updatePasswordPortalUser(userDetails.getUsername(), updatePasswordUserDTO);
        } catch (HttpServerErrorException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


        return ResponseEntity.ok().build();
    }

    /**
     * Endpoints that creates a new {@link User}
     *
     * @param registerUserDTO The {@link RegisterUserDTO} which contains the information about the user to create
     * @return The {@link ResponseEntity} with status {@code 201 (Created)} and with body the {@link User}
     * @throws BadRequestException {@code 400 (Bad Request)} if the data in the request body couldn't be validated
     * @throws ConflictException {@code 409 (Conflict)} if the userdata that should be unique, is not unique
     */
    @ApiOperation(
            value = "Create a new user"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created - The created user is returned"),
            @ApiResponse(code = 400, message = "Bad Request - The data in the request body was not valid"),
            @ApiResponse(code = 409, message = "Conflict - Creation failed due to conflict in username or email"),
    })
    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@Valid @RequestBody RegisterUserDTO registerUserDTO){
        log.info("[REST]: POST request made to /users");

        try {
            User user = authService.register(registerUserDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(user);
        } catch (HttpServerErrorException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
