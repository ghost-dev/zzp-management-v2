package nl.zzpmanagement.mailservice;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class MailServiceApplication {

    @Autowired
    private SendEmailService sendEmailService;

    public static void main(String[] args) {
        SpringApplication.run(MailServiceApplication.class, args);
    }

//    @EventListener(ApplicationReadyEvent.class)
//    public void triggerWhenStarts() throws Exception {
//        final String mailadres= "test@test.com";
//        byte[] attachment = createPdf().readAllBytes();
//        sendEmailService.sendEmail(mailadres,"Hi there!", "TEST");
//        sendEmailService.sendMailWithAttachment(mailadres,"Hi there!", "TEST see attachment","test.pdf",attachment);
//        sendEmailService.sendEmailToNewRegistratedUser(mailadres);
//    }

    public InputStream createPdf() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter writer = PdfWriter.getInstance(document, out);


        document.open();

        document.add(new Paragraph("My example PDF document"));
        // some logic here

        document.close();

        return new ByteArrayInputStream(out.toByteArray());
    }

}
